package main

import (
	"encoding/json"
	"errors"
)

type N struct {
	Id       int
	Val      string
	ParentId int
	Childs   []*N
}

func BuildTrees(ns []*N) (ret []*N, err error) {
	tbl := make(map[int]*N, len(ns))
	for _, item := range ns {
		if _, ok := tbl[item.Id]; ok {
			err = errors.New("item ID already exist")
			return
		}
		tbl[item.Id] = item
	}
	for _, item := range ns {
		if item.ParentId != -1 {
			obj, ok := tbl[item.ParentId]
			if !ok {
				err = errors.New("parent ID not exist")
				return
			}
			obj.Childs = append(obj.Childs, item)
		} else {
			ret = append(ret, item)
		}
	}
	return
}

func main() {
	nodes := []*N{
		&N{0, "val0", -1, nil},
		&N{1, "val1", 0, nil},
		&N{2, "val2", 1, nil},
		&N{3, "val3", 1, nil},
		&N{4, "val4", 0, nil},
		&N{5, "val5", 4, nil},
		&N{6, "val6", 4, nil},
		&N{7, "val7", -1, nil},
		&N{8, "val8", 7, nil},
		&N{9, "val9", 8, nil},
		&N{10, "val10", 8, nil},
		&N{11, "val11", 7, nil},
		&N{12, "val12", 11, nil},
		&N{13, "val13", 11, nil},
	}
	result, err := BuildTrees(nodes)
	if err != nil {
		println(err.Error())
		return
	}

	bs, err := json.MarshalIndent(result, "", "  ")
	if err != nil {
		println(err.Error())
		return
	}
	println(string(bs))
}
